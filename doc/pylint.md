# pylint

[Documentation](http://pylint.pycqa.org/en/latest/)

`pylint` is a static analyzer and your best friend (with a tough-love relationship).

Visual Studio Code encourages you to install `pylint` and start using it. :)

## Why use pylint
- To detect errors without having to execute the code quickly.
- To encourage good practices (ex: broad exception catching, order of imports, ...).
- To keep the code clean (ex: unused imports or variables, too many levels of indentation, ...).
- To standardize the formatting of the code base.
- To encourage you to write docstrings as you code! ;)

## Important for Python 2

pylint 2.0 dropped support for Python 2.

You can specify `pylint = "~=1.9"` in your Pipfile

## The pylintrc file

The `pylintrc` or `.pylintrc` file is used to configure pylint. In order to:
- Change certain parameters (ex: `max-line-length=160`)
- Disable certain messages. Tip: do the command `pylint --list-msgs > pylint-msgs.txt` to generate a file with all of pylint's messages.

How to generate the first version of your pylintrc file:
- `pylint --generate-rcfile > pylintrc`

## IDE Configuration

It is practical to add some additional configuration in your IDE to help handle, for example, trailing whitespaces.

Example for Visual Studio Code's `settings.json` file:
- `"files.trimTrailingWhitespace": true`

I recommend also looking into `autopep8` for more advanced and aggressive code formatting, especially if you have an exiting code base to adapt.
