# pytest

[Documentation](https://docs.pytest.org/en/latest/contents.html)

## WRITE YOUR UNIT TESTS

You can use whatever framework you want to write unit tests:
- pytest (most widely used)
- unittest (built-in)
- nose2
- ...

The most important, is to **write unit tests**.

It allows for others (including your future self!) to contribute to your project. It will:
- Help to avoid regressions
- Allow you to refactor the code
- Give examples of how to use your code

**So, write your unit tests!!**

## pytest integration tips

Put dependency information in your `setup.py`:
```python
...

SETUPTOOLS_REQUIREMENTS = [
    'setuptools_scm',
    'wheel',
    'pytest-runner',
]

TEST_REQUIREMENTS = [
    'pytest',
    'pytest-cov',
]

setup(
    ...,
    setup_requires=SETUPTOOLS_REQUIREMENTS,
    install_requires=PACKAGE_REQUIREMENTS,
    tests_require=TEST_REQUIREMENTS,
    ...,
)
```

Also, considerer using the `setup.cfg` file (standard file to give default options to setuptools):
```
[aliases]
test=pytest

[tool:pytest]
addopts=--cov=pytemplate --verbose
```

The above sample:
- Makes coverage run by default
- Makes you able to run `python setup.py test` (which will also collect test dependencies)

## And do not forget

 **Write your unit tests!!**
