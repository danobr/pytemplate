# pipenv

[Documentation](https://pipenv.readthedocs.io/en/latest/)

`pipenv` is a brilliant Python package for using the combination of `pip` and `virtualenv` in a user-friendly way.

## Why use virtual environements

Use either `pipenv` or `pip`+`virtualenv`, as long as you use virtual environments!
- [stackoverflow - What is a virtual env and why should I use one](https://stackoverflow.com/questions/41972261/what-is-a-virtualenv-and-why-should-i-use-one)
- [Python Tips - What is a virtualenv](https://pythontips.com/2013/07/30/what-is-virtualenv/)
- [David Fischer - Why you should be using pip and vritual env](https://www.davidfischer.name/2010/04/why-you-should-be-using-pip-and-virtualenv/)

## Workflow WITHOUT pipenv

```bash
pip install virtualenv  # If not already installed
virtualenv venv
source venv/bin/activate  # On Windows: venv\Scripts\activate.bat

# Start writting commands in virtualenv
pip install requirements.txt  # Install package dependencies
pip install pytest pytest-cov sphinx recommonmark sphinx-rtd-theme  # Install dev dependencies
pytest
cd doc
make html
```

## Workflow WITH pipenv

```bash
pip install virtualenv  # If not already installed
pipenv install --dev  # Install all package + dev dependencies
pipenv shell

# Start writting commands in virtualenv
pytest
cd doc
make html
```

## The Pipfiles

The combination of `Pipfile` and `Pipfile.lock` (from `pipenv`) is meant to replace the `requirements.txt`file (from `pip`).

See examples [here](https://pipenv.readthedocs.io/en/latest/basics/#example-pipfile-pipfile-lock).

The main advantage is to be able to separate your package's dependencies from your grandchildren dependencies. Execute `pipenv graph` to see!

You can generate a `requirements.txt` with the command `pipenv lock -r > requirements.txt`. This allows use to keep retro-compatibility with people that haven't adopted pipenv.
