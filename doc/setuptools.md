# setuptools (the setup.py file)

## Why?

It is important to consider your project as a package that others will use.

This allows you to adopt good practices:
- [Semantic versioning](https://semver.org/)
- Binary distribution (i.e. wheel) for installation on production environments
- Less boilerplate integration with CI

Having a setup.py file allows you to install, and **deploy** your package,
using either `python setup.py <command>` or `pip`.

```bash
# Several installation options:
python setup.py install
pip install .  # From the directory containing the setup.py file
pip install git+ssh://git@gitlab.com/danobr/pytemplate.git

python setup.py develop      # Super editable mode!
python setup.py bdist        # Source distribution
python setup.py bdist_wheel  # Binary distribution

pip uninstall pytemplate
```

Advantages:
- Specify dependencies so they are automatically installed
- Specify supported Python versions
- Add metadata to your package

Example:
```python
from setuptools import setup, find_packages

SETUPTOOLS_REQUIREMENTS = ['wheel', 'pytest-runner']
TEST_REQUIREMENTS = ['pytest', 'pytest-cov']
INSTALL_REQUIREMENTS = ['antigravity']

setup(
    name='pytemplate',
    description='Template for starting Python projects like a DevOps bawss!',
    setup_requires=SETUPTOOLS_REQUIREMENTS,
    install_requires=PACKAGE_REQUIREMENTS,
    tests_require=TEST_REQUIREMENTS,
    python_requires='>=2.7,!=3.0.*,!=3.1.*,!=3.2.*,!=3.3.*',
    packages=find_packages(),
)
```

## More Resources

- [Python Packaging User Guide](https://packaging.python.org/)
    - [Packaging and distributing projects](https://packaging.python.org/guides/distributing-packages-using-setuptools/)
- [How To Package Your Python Code](https://python-packaging.readthedocs.io/en/latest/index.html)
- [The Hitchhiker's Guide to Packaging](https://the-hitchhikers-guide-to-packaging.readthedocs.io/en/latest/creation.html)
- [Python Packaging Authority](https://www.pypa.io/en/latest/)
