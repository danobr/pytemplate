# setuptools_scm

Read the [GitHub README](https://github.com/pypa/setuptools_scm):
> the blessed package to manage your versions by scm tags

Basically, allows to use tags in Git in order to give a version number to your package.

Add this to `setup.py`:
```python
from setuptools import setup
setup(
    ...,
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    ...,
)
```

Put this in `__init__.py` file to access version from package:
```python
from pkg_resources import get_distribution, DistributionNotFound

try:
    __version__ = get_distribution(__name__).version
except DistributionNotFound:
    # package is not installed
    pass
```

Example:
```bash
> python -c "import pytemplate; print(pytemplate.__version__)"
0.1.dev35+g032c223
```
