.. pytemplate documentation master file, created by
   sphinx-quickstart on Fri Nov  2 04:05:23 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pytemplate's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   setuptools
   setuptools_scm
   pipenv
   pytest
   pylint
   sphinx


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`