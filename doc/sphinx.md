# sphinx

Generate documentation:
```bash
cd doc
.\make.bat html
```

Start a simple HTTP server:
```bash
cd doc\_build\html
python -m SimpleHTTPServer  #python3: python -m http.server
```

TODO:
- integrate autodoc for including docstrings
- GitLab Pages integration
