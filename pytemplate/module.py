"""Dymmy module inside Python package"""


def return_true():
    """Returns True

    Returns:
        bool -- Always True
    """
    return True
