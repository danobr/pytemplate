"""ALWAYS have a setup.py file!"""

from setuptools import setup, find_packages
try: # for pip >= 10
    from pip._internal.req import parse_requirements
except ImportError: # for pip <= 9.0.3
    from pip.req import parse_requirements

SETUPTOOLS_REQUIREMENTS = [
    'setuptools_scm',
    'wheel',
    'pytest-runner',
]

TEST_REQUIREMENTS = [
    'pytest',
    'pytest-cov',
]

# Use requirements.txt which is produced by pipenv's Pipfile
#   Command to install pipenv:
#     pip install pipenv
#   Command to regenerate requirements.txt:
#      pipenv lock -r > requirements.txt
PACKAGE_REQUIREMENTS = [
    str(each_req.req) for each_req in parse_requirements('requirements.txt', session=False)
]

setup(
    name='pytemplate',
    description='Template for starting Python projects like a DevOps bawss!',
    author='danobr',
    author_email='email@email.com',
    use_scm_version=True,
    setup_requires=SETUPTOOLS_REQUIREMENTS,
    install_requires=PACKAGE_REQUIREMENTS,
    tests_require=TEST_REQUIREMENTS,
    python_requires='>=2.7,!=3.0.*,!=3.1.*,!=3.2.*,!=3.3.*',
    packages=find_packages(),
    long_description=open('README.md').read(),
)
