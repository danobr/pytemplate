# README

![pipeline](https://gitlab.com/danobr/pytemplate/badges/master/pipeline.svg)
![coverage](https://gitlab.com/danobr/pytemplate/badges/master/coverage.svg)
![pylint](https://danobr.gitlab.io/pytemplate/pylint.svg)

## Project Objectives

- Learn about best practices in Python in terms of:
    - Monitoring code quality
    - Continuous integration
    - Continuous deployment
    - Documentation
    - Virtual environments
- Be able to start a new project quickly with solid foundations in terms of quality and maintainability
- Share this knowledge to whoever is interested

## Documentation

Read the [full documentation](https://danobr.gitlab.io/pytemplate) hosted on GitLab pages.

## Recommended dev set-up

Use [pipenv](https://pipenv.readthedocs.io/en/latest/)!

It is recommended to use **Powershell** under Windows for a better experience with `pipenv shell`.

Install pipenv:
- `pip install pipenv`

Go to project directory:
- `cd <project>`

Install all package + dev dependencies:
- `pipenv install --dev`

Activate virtualenv so next calls do not need to start with pipenv:
- `pipenv shell`

Install local source repo in **editable mode**:
- `python setup.py develop`

## Launch tests

Simple call to `pytest`:
- `pytest`

Use `setup.py` in order to install test requirements at the same time:
- `python setup.py test`

## Launch pylint

Call to pylint:
- `pylint pytemplate/ tests/*.py`
