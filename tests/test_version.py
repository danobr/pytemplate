"""Test package version number."""


import pytemplate


def test_version():
    """Test package version number."""
    assert pytemplate.__version__.startswith('0.')
